const got = require('got');
const config = require('./config');
const fs = require('fs');


/**
 * Array containing the days of the week in French
 * @type {string[]}
 */
const days = ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'];

/**
 * Array containing the months of the year in French
 * @type {string[]}
 */
const months = ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'];

/**
 * This method formats a date as follows YYYY-mm-dd
 *
 * @param date Date to be formatted
 * @returns {string}
 */
function formatDate(date) {
    return date.getFullYear() + '-' + String(date.getMonth() + 1).padStart(2, '0') + '-' + String(date.getDate()).padStart(2, '0');
}

/**
 * This method formats hour as follows HH:mm
 *
 * @param date Date to be formatted
 * @returns {string}
 */
function formatHour(date) {
    return date.getHours() + 'h' + String(date.getMinutes()).padStart(2, '0');
}

/**
 * This method empties the configuration files related to the activities
 */
function truncateActivitiesFile() {
    for (let idx = 0; idx < 3; idx++) {
        fs.truncate(config.HUB_TITLE_FILE.replace('{%}', (idx + 1).toString()), 0, (err) => {
        })
        fs.truncate(config.HUB_LOCATION_FILE.replace('{%}', (idx + 1).toString()), 0, () => {
        })
        fs.truncate(config.HUB_DATE_FILE.replace('{%}', (idx + 1).toString()), 0, () => {
        })
        fs.truncate(config.HUB_HOUR_FILE.replace('{%}', (idx + 1).toString()), 0, () => {
        });
    }
}

/**
 * This method allows you to write an activity in the associated configuration file
 *
 * @param file Name of the configuration file
 * @param element Activity
 */
function writeActivityInFile(file, element) {
    fs.writeFile(file, element, error => {
        if (error) {
            console.error('An error has occurred during during the writing process of the details of the activity!', error);
        } else
            console.log('The file has been updated!');
    });
}

/**
 * This method allows you to iterate through an array of activities to write the date, time, title and location of these activities
 * in the associated configuration files in the associated configuration files
 *
 * @param activities
 */
function writeActivities(activities) {
    let title, room, date, formattedDate, formattedHour = null;

    for (let idx = 0; idx < activities.length; idx++) {
        date = new Date(activities[idx]['start']);
        formattedHour = formatHour(date);
        formattedDate = days[date.getDay()] + ' ' + date.getDate() + ' ' + months[date.getMonth()];
        room = activities[idx]['room']['code'].split(/[\/,]+/).pop();
        title = (activities[idx]['acti_title'].match(/.{20}\w*\S*|.*/g)).map(x => x.trim()).join('\n');

        writeActivityInFile(config.HUB_TITLE_FILE.replace('{%}', (idx + 1).toString()), title);
        writeActivityInFile(config.HUB_LOCATION_FILE.replace('{%}', (idx + 1).toString()), room.replace(/[\-]/gi, ' '));
        writeActivityInFile(config.HUB_DATE_FILE.replace('{%}', (idx + 1).toString()), formattedDate);
        writeActivityInFile(config.HUB_HOUR_FILE.replace('{%}', (idx + 1).toString()), formattedHour);
    }
}

/**
 * This method allows you to retrieve the first and the last days of the current week
 *
 * @returns {{start: string, end: string}}
 */
function getFirstAndLastDaysOfWeek() {
    let today = new Date();
    let firstDay = today.getDate() - today.getDay() + 1;

    return {
        start: formatDate(new Date(today.setDate(firstDay))),
        end: formatDate(new Date(today.setDate(firstDay + 6)))
    };
}

/**
 * This method allows you to retrieve the activities of the current week
 */
function getActivitiesOfWeek() {
    let activities = [];
    let options = getFirstAndLastDaysOfWeek();

    got(config.AUTOLOGIN_URL + config.PLANNING_URL, {
        method: 'GET',
        responseType: 'json',
        searchParams: {format: 'json', ...options}
    })
        .then(response => {
            activities = response.body.filter(function (item) {
                return item['titlemodule'] === config.MODULE_TITLE &&
                    item['instance_location'] === config.MODULE_INSTANCE &&
                    new Date(options.start) <= new Date(item['start'])
            });
            activities.sort((a, b) => new Date(a['start']) - new Date(b['start']));

            truncateActivitiesFile();
            writeActivities(activities.slice(0, 3));

        })
        .catch(error => console.error(`${error} - An error occurred during during recovery from activities!`));
}

module.exports = {
    getActivitiesOfWeek
}
