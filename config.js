module.exports = {
    /**
     * Path of the CSV file containing students' names and dates of birth
     * @type {string}
     */
    BIRTHDAYS_SOURCES: 'sources/birthdays.csv',

    /**
     * Path of the CSV file containing a list of citations and authors' names
     * @type {string}
     */
    QUOTES_SOURCES: 'sources/quotes.csv',

    /**
     * Path of the file in which to write the names of students whose current day is their birthday
     * @type {string}
     */
    BIRTHDAY_FILE: 'files/birthday.txt',

    /**
     * Path of the file in which to write the randomly selected quote
     * @type {string}
     */
    QUOTE_FILE: 'files/quote.txt',

    /**
     * Path of the file in which to write the author of the selected quote
     * @type {string}
     */
    QUOTE_AUTHOR_FILE: 'files/quote_author.txt',

    /**
     * Path of the file in which to write the activity date
     * @type {string}
     */
    HUB_DATE_FILE: 'files/hub_date{%}.txt',

    /**
     * Path of the file in which to write the activity hours
     * @type {string}
     */
    HUB_HOUR_FILE: 'files/hub_heure{%}.txt',

    /**
     * Path of the file in which to write the activity location
     * @type {string}
     */
    HUB_LOCATION_FILE: 'files/hub_loc{%}.txt',

    /**
     * Path of the file in which to write the activity title
     * @type {string}
     */
    HUB_TITLE_FILE: 'files/hub_titre{%}.txt',

    /**
     * Autologin url retrieve in the administration part of the EPITECH intranet
     * @type {string}
     */
    AUTOLOGIN_URL: 'https://intra.epitech.eu/auth-957bcb00bb97c71ec00d0a90f1aa5353b438d09a',

    /**
     * Path to the schedule in order to retrieve the week's activities
     * @type {string}
     */
    PLANNING_URL: '/planning/load',

    /**
     * Title of the module of activities to be retrieved
     * @type {string}
     */
    MODULE_TITLE: 'B0 - Hub',

    /**
     * Instance code of the activity module to be retrieved
     * @type {string}
     */
    MODULE_INSTANCE: 'FR/BDX'
};
