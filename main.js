const csv = require('csv-parser');
const fs = require('fs');
const config = require('./config');
const workshop = require('./workshop');


/**
 * This method formats a date as follows dd/mm (example: 08/08)
 *
 * @param date Date to be formatted
 * @returns {string}
 */
function formatDate(date) {
    return String(date.getDate()).padStart(2, '0') + '/' + String(date.getMonth() + 1).padStart(2, '0');
}

/**
 * This method allows you to change the birthday of a student by keeping only the day and month
 *
 * @param data
 * @param birthdays
 */
function writeStudentBirthday(data, birthdays) {
    if ('birthday' in data) {
        data['birthday'] = data['birthday'].slice(0, 5);
        birthdays.push(data);
    }
}

/**
 * This method selects the names of students whose current day is their birthday and writes them to the associated configuration file.
 *
 * @param birthdays
 */
function writeStudentName(birthdays) {
    let names = '';
    const today = formatDate(new Date());
    const students = birthdays.filter(item => item['birthday'] === today);

    for (let idx = 0; idx < students.length; idx++) {
        names += students[idx]['student'] + '\n';
    }
    fs.writeFile(config.BIRTHDAY_FILE, names, error => {
        if (error)
            console.error('An error has occurred during the writing process of student names!', error);
        else
            console.log('The file has been updated!');
    });
}

/**
 * This method randomly selects a quote and writes it to the associated configuration file.
 *
 * @param quotes
 */
function writeQuote(quotes) {
    let item = quotes[Math.floor(Math.random() * quotes.length)];
    let quote = (item['quote'].match(/.{35}\w*\S*|.*/g)).map(x => x.trim()).join('\n');
    
    quote = quote.replace(new RegExp(String.fromCharCode(160), "g"), ' ');    
    fs.writeFile(config.QUOTE_FILE, quote, error => {
        if (error)
            console.error('An error has occurred during the writing process of the quote!', error);
        else
            console.log('The file has been updated!');
    });

    fs.writeFile(config.QUOTE_AUTHOR_FILE, item['author'], error => {
        if (error)
            console.error('An error has occurred during the writing process of the author of the quote!', error);
        else
            console.log('The file has been updated!');
    });
}

/**
 * This method retrieves a list of students containing their first name, last name and date of birth.
 * It parses the CSV file line by line to retrieve the students whose current day is their birthday.
 */
function getStudentByBirthday() {
    const birthdays = [];

    fs.createReadStream(config.BIRTHDAYS_SOURCES, {encoding: 'binary'})
        .pipe(csv({separator: ';'}))
        .on('data', (data) => writeStudentBirthday(data, birthdays))
        .on('end', () => writeStudentName(birthdays));
}

/**	
 * This method retrieves a list of quotes and thier authors.
 * It parses the CSV file line by line to retrieve a quote in a random way. 
 */
function getRandomQuote() {
    const quotes = [];

    fs.createReadStream(config.QUOTES_SOURCES, {encoding: 'binary'})
    	.pipe(csv({separator: ';'}))
        .on('data', (data) => quotes.push(data))
        .on('end', () => writeQuote(quotes));
}

/**
 * Function called when the script is launched
 */
function main() {
    getStudentByBirthday();
    getRandomQuote();
    workshop.getActivitiesOfWeek()
}

main();
